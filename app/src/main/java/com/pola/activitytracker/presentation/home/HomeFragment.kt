package com.pola.activitytracker.presentation.home

import android.content.Context
import android.hardware.SensorManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.pola.activitytracker.R
import com.pola.activitytracker.base.BaseFragment
import com.pola.activitytracker.databinding.FragmentHomeBinding
import com.pola.activitytracker.utils.StepSensorHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {

    private lateinit var stepSensorHelper: StepSensorHelper
    private var initialStepCount = -1
    private var currentStepCount = 0

    private var isTracking = false

    override fun initBinding(): FragmentHomeBinding =
        FragmentHomeBinding.inflate(layoutInflater)

    override fun initViewModel(): HomeViewModel =
        ViewModelProvider(this)[HomeViewModel::class.java]

    override fun onFragmentCreated() {
        initStepSensor()

        binding.fab.setOnClickListener {
            binding.fab.isExpanded = true
            startTracking()
        }

        binding.btnStop.setOnClickListener {
            binding.fab.isExpanded = false
            stepSensorHelper.stopTracking()
        }

        binding.btnPausePlay.setOnClickListener {
            if (isTracking) pauseTracking()
            else startTracking()
        }
    }

    private fun initStepSensor() {
        val sensorManager =
            requireActivity().getSystemService(Context.SENSOR_SERVICE) as SensorManager
        stepSensorHelper = StepSensorHelper(sensorManager) { steps ->
            if (initialStepCount == -1) initialStepCount = steps.toInt()

            currentStepCount = steps.toInt() - initialStepCount
            updateStepsCount()
        }
    }

    private fun pauseTracking() {
        isTracking = false
        stepSensorHelper.stopTracking()

        binding.btnPausePlay.apply {
            icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_round_play_arrow_24)
            text = "Resume"
        }
    }

    private fun startTracking() {
        isTracking = true
        stepSensorHelper.startTracking()

        binding.btnPausePlay.apply {
            icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_round_pause_24)
            text = "Pause"
        }
    }

    private fun updateStepsCount() {
        binding.tvSteps.text = "$currentStepCount"
    }
}