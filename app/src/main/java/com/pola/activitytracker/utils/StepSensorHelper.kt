package com.pola.activitytracker.utils

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager

class StepSensorHelper(
    private val sensorManager: SensorManager? = null,
    private val onStepUpdates: (steps: Float) -> Unit,
) : SensorEventListener {

    fun startTracking() {
        val stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        stepsSensor?.let {
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_FASTEST)
        } ?: return
    }

    fun stopTracking() {
        sensorManager?.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.values?.firstOrNull()?.let(onStepUpdates)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }
}