package com.pola.activitytracker.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<V : ViewBinding, VM : BaseViewModel> : Fragment() {

    private var _binding: V? = null
    val binding get() = _binding!!

    open lateinit var viewModel: VM

    abstract fun initBinding(): V

    abstract fun initViewModel(): VM

    abstract fun onFragmentCreated()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        super.onCreate(savedInstanceState)

        viewModel = initViewModel()
        _binding = initBinding()

        onFragmentCreated()

        return binding.root
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}